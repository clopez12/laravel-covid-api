<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class CCAASeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ccaas')->insert([
            'nombre' => 'Cataluña',
            'pais_id' => 1,
        ]);
    }
}
