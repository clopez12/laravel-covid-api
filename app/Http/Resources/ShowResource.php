<?php

namespace App\Http\Resources;

use App\Models\CCAAs;
use App\Models\Ia14;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->numero){
            $pais = DB::table('paises')->join('ccaas','paises.id','=','ccaas.pais_id')
                ->select('paises.*')->first();
            $ccaa = CCAAs::where('id',$this->ccaas_id)->first();
            return [
                'fecha' => $this->fecha,
                'pais' => $pais->nombre,
                'ccaas_id' => $ccaa->nombre,
                'value' =>$this->numero,
            ];
        }else{
            $pais = DB::table('paises')->join('ccaas','paises.id','=','ccaas.pais_id')
                ->select('paises.*')->first();
            $ccaa = CCAAs::where('id',$this->ccaas_id)->first();
            return [
                'fecha' => $this->fecha,
                'pais' => $pais->nombre,
                'ccaas_id' => $ccaa->nombre,
                'value' =>$this->incidencia,
            ];
        }

    }
}
