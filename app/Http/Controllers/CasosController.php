<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\Casos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $casos = new Casos();
        $casos->fecha = $request->fecha;
        $casos->ccaas_id = $request->ccaas_id;
        $casos->numero = $request->numero;
        $casos->save();
        return response()->json($casos);
    }

    /**
     * Display the specified resource.
     *
     * @
     * @return \Illuminate\Http\Response
     */

    public function showAll()
    {
        $casos = Casos::all();
        if(!$casos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$casos],200);
    }

    public function show($id)
    {
        $casos = DB::select(DB::raw("select * from casos where fecha='$id'"));
        if(!$casos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$casos],200);
    }
    public function showCollection($id,$id2)
    {
        $casos = DB::select(DB::raw("SELECT * from casos where fecha between '$id' and '$id2'"));
        if(!$casos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return new CovidCollection($casos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->fecha;
        $request->ccaas_id;
        $request->numero;
        Casos::where('fecha', $request->fecha)
            ->update(['fecha' =>  $request->nueva_fecha,
                'ccaas_id' =>  $request->ccaas_id,
                'numero' =>  $request->numero
            ]);
        $casos = DB::select(DB::raw("select * from casos where fecha='$request->nueva_fecha'"));
        if(!$casos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'updated','data'=>$casos],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
