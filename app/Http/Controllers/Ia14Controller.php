<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\Ia14;
use App\Http\Resources\Ia14Resource;
use Illuminate\Http\Request;
use DB;

class Ia14Controller extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ia14 = new Ia14();
        $ia14->fecha = $request->fecha;
        $ia14->ccaas_id = $request->ccaas_id;
        $ia14->incidencia = $request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }

    /**
     * Display the specified resource.
     *
     * @
     * @return \Illuminate\Http\Response
     */

    public function showAll()
    {
        $ia14 = Ia14::all();
        if(!$ia14){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$ia14],200);
    }

    public function show($id)
    {
        $ia14 = DB::select(DB::raw("select * from ia14 where fecha='$id'"));
        if(!$ia14){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$ia14],200);
    }
    public function showCollection($id,$id2)
    {
        $ia14 = DB::select(DB::raw("SELECT * from ia14 where fecha between '$id' and '$id2'"));
        if(!$ia14){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return new CovidCollection($ia14);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->fecha;
        $request->id;
        $request->ccaas_id;
        $request->incidencia;
        Ia14::where('fecha', $request->fecha)
            ->update([
                'ccaas_id' =>  $request->ccaas_id,
                'fecha' =>  $request->nueva_fecha,
                'incidencia' =>  $request->incidencia
            ]);
        $ia7 = DB::select(DB::raw("select * from ia14 where fecha='$request->nueva_fecha'"));
        if(!$ia7){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se ha cambiado correctamente'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$ia7],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
