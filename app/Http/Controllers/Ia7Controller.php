<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\Ia7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ia7 = new Ia7();
        $ia7->fecha = $request->fecha;
        $ia7->ccaas_id = $request->ccaas_id;
        $ia7->incidencia = $request->incidencia;
        $ia7->save();
        return response()->json($ia7);
    }

    /**
     * Display the specified resource.
     *
     * @
     * @return \Illuminate\Http\Response
     */

    public function showAll()
    {
        $ia7 = Ia7::all();
        if(!$ia7){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$ia7],200);
    }

    public function show($id)
    {
        $ia7 = DB::select(DB::raw("select * from ia7 where fecha='$id'"));
        if(!$ia7){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$ia7],200);
    }
    public function showCollection($id,$id2)
    {
        $ia7 = DB::select(DB::raw("SELECT * from ia7 where fecha between '$id' and '$id2'"));
        if(!$ia7){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return new CovidCollection($ia7);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->fecha;
        $request->id;
        $request->ccaas_id;
        $request->incidencia;
        Ia7::where('fecha', $request->fecha)
            ->update([
                'ccaas_id' =>  $request->ccaas_id,
                'fecha' =>  $request->nueva_fecha,
                'incidencia' =>  $request->incidencia
            ]);
        $ia7 = DB::select(DB::raw("select * from ia7 where fecha='$request->nueva_fecha'"));
        if(!$ia7){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se ha cambiado correctamente'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$ia7],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
