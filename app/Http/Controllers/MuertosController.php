<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\Muertos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MuertosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $muertos = new Muertos();
        $muertos->fecha = $request->fecha;
        $muertos->ccaas_id = $request->ccaas_id;
        $muertos->numero = $request->numero;
        $muertos->save();
        return response()->json($muertos);
    }

    /**
     * Display the specified resource.
     *
     * @
     * @return \Illuminate\Http\Response
     */

    public function showAll()
    {
        $muertos = Muertos::all();
        if(!$muertos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$muertos],200);
    }

    public function show($id)
    {
        $muertos = DB::select(DB::raw("select * from muertos where fecha='$id'"));
        if(!$muertos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'ok','data'=>$muertos],200);
    }
    public function showCollection($id,$id2)
    {
        $muertos = DB::select(DB::raw("SELECT * from muertos where fecha between '$id' and '$id2'"));
        if(!$muertos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return new CovidCollection($muertos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->fecha;
        $request->ccaas_id;
        $request->numero;
        Muertos::where('fecha', $request->fecha)
            ->update(['fecha' =>  $request->nueva_fecha,
                'ccaas_id' =>  $request->ccaas_id,
                'numero' =>  $request->numero
            ]);
        $muertos = DB::select(DB::raw("select * from muertos where fecha='$request->nueva_fecha'"));
        if(!$muertos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        return response()->json(['status' => 'updated','data'=>$muertos],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
