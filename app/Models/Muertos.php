<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Muertos extends Model
{
    use HasFactory;
    protected $table = 'muertos';
    public $timestamps = false;
    protected $fillable = ['fecha','ccaas_id','numero'];
}
